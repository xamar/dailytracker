﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DailyTracker
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        public void EnterButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ActivitiesDisplayPage());
        }
	}
}
