﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DailyTracker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivitiesDisplayPage : TabbedPage
    {
        public ActivitiesDisplayPage ()
        {
            InitializeComponent();
        }

        public void AddActivityButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddActivityPage());
        }
    }
}