﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DailyTracker
{
	public partial class App : Application
	{
        public static string DatabaseLocation = string.Empty;

		public App ()
		{
			InitializeComponent();

			MainPage = new NavigationPage(new DailyTracker.MainPage());
		}

        public App(string datebaseLocation)
        {
            InitializeComponent();

            MainPage = new NavigationPage(new DailyTracker.MainPage());

            DatabaseLocation = datebaseLocation;
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
