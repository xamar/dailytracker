﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DailyTracker
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddActivityPage : ContentPage
	{
		public AddActivityPage ()
		{
			InitializeComponent ();

           var items = new List<string>
            {
                "Daily","Weekly","Monthly"
            };
            foreach(var item in items)
                repetitionPicker.Items.Add(item);
            measurementPicker.Items.Add("Repetitions per day");
            measurementPicker.Items.Add("Time spent in hours");
            measurementPicker.Items.Add("Time spent in minutes");
            measurementPicker.Items.Add("Yes or No");
        }
        public void SaveButton_Clicked(object sender, EventArgs e)
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Activity>();

                Activity activity = new Activity()
                {
                    Duration = repetitionPicker.SelectedIndex,
                    Measurement = measurementPicker.SelectedIndex,
                    Name = activityNameEntry.Text
                };

                conn.Insert(activity);
            }

            Navigation.PushAsync(new ActivitiesDisplayPage());
        }
    }
}