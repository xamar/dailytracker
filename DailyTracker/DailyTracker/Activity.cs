﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SQLite;

namespace DailyTracker
{

    public class Activity : INotifyPropertyChanged
    {
        //[PrimaryKey, AutoIncrement]
        //public int Id { get; set; }
        public int measurement;
        public int Measurement
        {
            get { return measurement; }
            set { measurement = value; OnPropertyChanged("Measurement"); }
        }

        public string name { get; set; }
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        public DateTime DateStarted { get; set; }
        public DateTime TargetDate { get; set; }
        public int Duration { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    public class Habit : Activity
    {
        public int count { get; set; }
        public int Count
        {
            get { return count; }
            set { count = value; OnPropertyChanged("Count"); }
        }

        public bool isDone { get; set; }
        public bool IsDone
        {
            get { return isDone; }
            set { isDone = value; OnPropertyChanged("IsDone"); }
        }

        public DateTime ViewDate { get; set; }
    }
}
