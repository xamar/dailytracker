﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DailyTracker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DayListPage : ContentPage
    {
        public string[] Units = { "Reps", "Minutes", "Hours" };
        public List<string> Items { get; set; }
        public DayListPage()
        {
            InitializeComponent();

            Style listLabelStyle = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter{Property= Label.FontSizeProperty, Value=24},
                    new Setter{Property= Label.TextColorProperty, Value=Color.DodgerBlue},
                    new Setter{Property=Label.VerticalOptionsProperty, Value=LayoutOptions.Center}
                }
            };
            Resources = new ResourceDictionary();
            Resources.Add("listLabelStyle", listLabelStyle);
            int i = 0;
            using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            {
                conn.CreateTable<Activity>();
                var activities = conn.Table<Activity>().ToList();
                Button save = new Button { Text = "save" };
                Switch yesNo = new Switch();
                
                Label slabel = new Label();

                for (int k=0;k<activities.Count;k++) 
                {
                    activityGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(36) });
                    Label name = new Label()
                    {
                        Text = activities[k].Name,
                        Style = (Style)Resources["listLabelStyle"]
                    };
                    activityGrid.Children.Add(name, 0, i);
                    Entry measureCount = new Entry { Keyboard = Keyboard.Numeric, Text = {  } };
                    if (activities[k].Measurement == 3)
                    {
                        activityGrid.Children.Add(yesNo, 1, i);
                        activityGrid.Children.Add(slabel, 2, i);
                    }
                    else
                    {
                        activityGrid.Children.Add(measureCount, 1, i);
                        activityGrid.Children.Add(new Label { Text = Units[activities[k].Measurement],
                            VerticalOptions = LayoutOptions.EndAndExpand }, 2, i);
                    }
                    i++;

                    save.Clicked += delegate
                    {
                        //using (SQLiteConnection con = new SQLiteConnection(App.DatabaseLocation))
                        //{
                            conn.CreateTable<Habit>();
                            foreach (var activity in activities)
                            {
                                Habit habit = new Habit();
                                habit.Name = activity.Name;
                                habit.Measurement = activity.Measurement;
                                habit.ViewDate = DateTime.Today;
                                if (activity.Measurement == 0)
                                    habit.IsDone = yesNo.IsToggled;
                                else
                                    habit.Count = Convert.ToInt32(measureCount.Text);
                                conn.Insert(habit);
                            }
                        //}
                    };
                }
                activityGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(36) });
                activityGrid.Children.Add(save, 0, i);
                Grid.SetColumnSpan(save, 3);                  
            }
        }        
    }
}
